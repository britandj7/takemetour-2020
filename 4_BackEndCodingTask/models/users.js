const root = require('path').dirname(require.main.filename);
const config = require(`${root}/config`)
const url = config.mongoUrl
const mongoose = require('mongoose'); mongoose.set('useCreateIndex', true);
const con = mongoose.createConnection(url, { useNewUrlParser: true, useUnifiedTopology: true });
const uniqueValidator = require('mongoose-unique-validator')
const timestamps = require('mongoose-timestamp')
const bcrypt = require('bcrypt');


const mangoSchema = mongoose.Schema({
	user: { type: String, required: true },
	pass: { type: String, required: true },
})

mangoSchema.index(
	{ user: 1 },
	{ unique: true }
);

mangoSchema.statics = {
	createUsers: async function () {
		try {		
					const chkUser = await this.countDocuments()
					if (!chkUser) {
						const users = [
							{ user: 'aaaaaa', pass: '111111' },
							{ user: 'bbbbbb', pass: '222222' },
							{ user: 'cccccc', pass: '333333' },
							{ user: 'dddddd', pass: '444444' },
						]
						for (let user of users) {
								user.pass = bcrypt.hashSync(user.pass,10); // true
								await this.create(user)
						}
					}else{
						console.log('Not thing');
					}					

		} catch (e) {
				console.log(e,'catch on createUsers');
		}
	},
	chkUser: async function (user,pass) {
		try {		
					const rs_user = await this.findOne({user:user},{user:1,pass:1});
					if(!rs_user){
						return false
					}else{
						return bcrypt.compareSync(pass, rs_user.pass);
					}
		} catch (e) {
				console.log(e,'catch on chkUser');
				return false
		}
	},
}

mangoSchema.plugin(uniqueValidator)
mangoSchema.plugin(timestamps)

module.exports = con.model('users', mangoSchema);
