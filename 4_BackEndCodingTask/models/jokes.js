const root = require('path').dirname(require.main.filename);
const config = require(`${root}/config`)
const url = config.mongoUrl
const mongoose = require('mongoose'); mongoose.set('useCreateIndex', true);
const con = mongoose.createConnection(url, { useNewUrlParser: true, useUnifiedTopology: true });
const uniqueValidator = require('mongoose-unique-validator')
const timestamps = require('mongoose-timestamp')

const mangoSchema = mongoose.Schema({
	id: { type: Number, required: true },
	joke: { type: String, required: true },
	likeStatus: { type: Number, default: 0 },
})

mangoSchema.index(
	{ joke: 1 },
	{ unique: true }
);

mangoSchema.statics = {
	maxId: async function () {
		const rs = await this.findOne({}, { id: 1 }).sort({ id: -1 });
		return rs && rs.id || 0
	},
}

mangoSchema.plugin(uniqueValidator)
mangoSchema.plugin(timestamps)

module.exports = con.model('jokes', mangoSchema);

