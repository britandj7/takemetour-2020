const express = require('express')
const root = require('path').dirname(require.main.filename);
const chklogin = require(`${root}/middleware/chklogin`)
const controller = require(`${root}/controllers/joke`)
const router = express.Router()

router.get('/',controller.getAllJoke)
router.post('/',chklogin,controller.addNewJoke)
router.get('/:id',chklogin,controller.getJokeById)
router.delete('/:id',chklogin,controller.deleteJoke)
router.post('/:id/like',chklogin,controller.likeJoke)
router.post('/:id/dislike',chklogin,controller.dislikeJoke)
module.exports = router
