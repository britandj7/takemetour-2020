const root = require('path').dirname(require.main.filename);
const auth = require('basic-auth')
const USERS = require(root + '/models/users')
module.exports = async (req, res, next) => {
			try {
				const user = auth(req)
				const chkUser = await USERS.chkUser(user.name, user.pass)
				if(chkUser)next()
				else res.sendStatus(403)
			} catch (e) {
				console.log(e,'catch on chk auth');
				res.sendStatus(403)
			}
}
