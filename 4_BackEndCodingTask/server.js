
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.SERVER_PORT || 3300
// const USERS = require('./models/users') // create id only start and no users
const app = express()
app.use(bodyParser.json())
app.use("/joke",require('./routers/joke'))
app.get('*', async function (_, res) {
	res.sendStatus(404)
})
const server = http.createServer(app)
server.listen(port, async function () {
	// await USERS.createUsers()  // create id only start and no users
	console.log('Start At Port 	: ', port)
})
