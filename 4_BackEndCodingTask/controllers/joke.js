const root = require('path').dirname(require.main.filename);
const JOKES = require(`${root}/models/jokes`)

module.exports.getAllJoke = async (_, res) => {
	try {
		
		const rs = await JOKES.aggregate([
			{
				$addFields: {
					likeText: {
						$switch: {
							branches: [
								{ case: { $eq: ['$likeStatus', 1] }, then: "Liked" },
								{ case: { $eq: ['$likeStatus', 2] }, then: "Disliked" },
							],
							default: "None"
						}
					}
				},
			},
			{$project:{
					"id":1,
					"joke":1,
					"likeStatus":1,
					"likeText":1,
					"createdAt":1,
			}},
			{ $sort: { createdAt: -1 } }
		])
		res.send(rs)
	} catch (e) {
		console.log(e, 'catch on getAllJoke');
		res.sendStatus(503)
	}
}

module.exports.addNewJoke = async (req, res) => {
	try {
		const data = req.body
		const maxId = await JOKES.maxId()
		const chkJoke = await JOKES.countDocuments({ joke: data.joke })
		if (chkJoke) {
			res.send({ status: false, msg: 'This Joke Is Exists.' })
		} else {
			const rs = await JOKES.create({ ...data, ...{ id: maxId + 1 } })
			if (rs) res.send({ status: true, msg: 'Add New Joke Success' })
			else res.send({ status: false, msg: 'Add New Joke Fail' })
		}
	} catch (e) {
		console.log(e, 'catch on addNewJoke');
		res.sendStatus(503)
	}
}

module.exports.getJokeById = async (req, res) => {
	try {
		const id = req.params.id
		const rs = await JOKES.findOne({ id: id },{"joke":1,"likeStatus":1,"likeText":1,"createdAt":1,})
		if(rs)res.send(rs)
		else res.send({status:false,msg:'Not Found Joke'})
	} catch (e) {
		console.log(e, 'catch on getJokeById');
		res.sendStatus(503)
	}
}

module.exports.deleteJoke = async (req, res) => {
	try {
		const id = req.params.id
		const chkJoke = await JOKES.countDocuments({ id: id })
		if (chkJoke) {
			const rs = await JOKES.deleteOne({ id: id })
			if (rs.n) res.send({ status: true, msg: 'Delete Joke Success.' })
			else res.send({ status: false, msg: 'Delete Joke Fail.' })
		} else {
			res.send({ status: false, msg: 'Not Found Joke' })
		}
	} catch (e) {
		console.log(e, 'catch on deleteJoke');
		res.sendStatus(503)
	}
}

module.exports.likeJoke = async (req, res) => {
	try {
		const id = req.params.id
		const chkJoke = await JOKES.findOne({ id: id },{likeStatus:1})
		if (chkJoke) {
			const likeStatus = chkJoke.likeStatus === 1 ? 0 : 1
			const msg = likeStatus? 'Liked' : 'Unliked'
			const rs = await JOKES.updateOne({ id: id }, { $set: { likeStatus: likeStatus } })
			if (rs.n) res.send({ status: true, msg:`${msg} Success.`})
			else res.send({ status: false, msg: `${msg} Fail.` })
		} else {
			res.send({ status: false, msg: 'Not Found Joke' })
		}
	} catch (e) {
		console.log(e, 'catch on likeJoke');
		res.sendStatus(503)
	}
}

module.exports.dislikeJoke = async (req, res) => {
	try {
		const id = req.params.id
		const chkJoke = await JOKES.findOne({ id: id },{likeStatus:1})
		if (chkJoke) {
			const likeStatus = chkJoke.likeStatus === 2 ? 0 : 2
			const msg = likeStatus? 'Disliked' : 'Undisliked'
			const rs = await JOKES.updateOne({ id: id }, { $set: { likeStatus: likeStatus } })
			if (rs.n) res.send({ status: true, msg:`${msg} Success.`})
			else res.send({ status: false, msg: `${msg} Fail.` })
		} else {
			res.send({ status: false, msg: 'Not Found Joke' })
		}
	} catch (e) {
		console.log(e, 'catch on dislikeJoke');
		res.sendStatus(503)
	}
}

