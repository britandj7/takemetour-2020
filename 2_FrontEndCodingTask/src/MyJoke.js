import React, { useState } from 'react';
import { MDBInput, MDBRadio, MDBRow, MDBCol, MDBBtn, MDBContainer } from 'mdb-react-ui-kit';
function MyJoke() {
	const options = [1, 2, 3, 4];
	const [jokeFilter, setJokFilter] = useState({ jokeNumber: 1, firstName: '', lastName: '' })
	const [jokeResult, setJokeResult] = useState([])
	const [btnDisabled, setBtnDisabled] = useState(false)

	function jokeNumberChange(event) {
		const value = event.target.value
		setJokFilter({ ...jokeFilter, ...{ jokeNumber: value } })
	}

	function jokeFirstNameChange(event) {
		const value = event.target.value
		setJokFilter({ ...jokeFilter, ...{ firstName: value } })
	}

	function jokeLastNameChange(event) {
		const value = event.target.value
		setJokFilter({ ...jokeFilter, ...{ lastName: value } })
	}

	async function getNewJoke() {
		setBtnDisabled(true)
		let url = `http://api.icndb.com/jokes/random/${jokeFilter.jokeNumber}?escape=javascript`;
		if (jokeFilter.firstName) url += `&firstName=${jokeFilter.firstName}`;
		if (jokeFilter.lastName) url += `&lastName=${jokeFilter.lastName}`;
		fetch(url).then(res => res.json())
			.then((result) => {
				setJokeResult(result.value)
				setBtnDisabled(false)
			})
			.catch((e) => {
				console.log(e, 'catch');
				setBtnDisabled(false)
			})
	}



	return (
		<div className="MyForm">
			<MDBContainer>
				<MDBRow className="mt-2">
					<MDBCol size='2'>
						<MDBRow className="MyForm__Title">Joke Number</MDBRow>
						<MDBRow className="MyForm__Option">
							{
								options.map((i, k) => {
									return (
										<MDBRadio className="" name="JokeOptions" key={k} label={i} value={i} onChange={jokeNumberChange} checked={jokeFilter.jokeNumber === i ? true : false} />
									)
								})
							}
						</MDBRow>
					</MDBCol>
					<MDBCol size='3'>
						<MDBRow className="MyForm__Title">Joke Filter</MDBRow>
						<MDBInput className="MyForm__Input" label="First Name" onChange={jokeFirstNameChange} placeholder="Optional" />
						<MDBInput className="MyForm__Input" label="Last Name" onChange={jokeLastNameChange} placeholder="Optional" />
					</MDBCol>
				</MDBRow>
				<MDBRow className="MyForm__Btn">
					<MDBBtn onClick={getNewJoke} disabled={btnDisabled}>Get Joke</MDBBtn>
				</MDBRow>
				<hr hidden={jokeResult.length === 0}></hr>
				<MDBRow className="MyForm__Title" hidden={jokeResult.length === 0}>Joke Render</MDBRow>
				<ul>
					{jokeResult.map((i, k) => {
						return (
							<li key={k} >{i.joke}</li>
						);
					})}
				</ul>
			</MDBContainer>
		</div>
	);
}

export default MyJoke;
