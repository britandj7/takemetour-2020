function fib(number = 1) {
	let a = 0, b = 1, temp = 0;
	for (let i = 1; i <= number; i++) {
		temp = a + b;
		a = b;
		b = temp;
	}
	return temp
}

function fib2(number) {
	if (number <= 1) return 1;
	return fib2(number - 1) + fib2(number - 2);
}

function shift(ar = ['1', '2', '3', '4', '5'], shift = 'right', number = 2) {
	for (let i = 0; i < number; i++) {
		if (shift == 'right') {
			const temp = ar.pop();
			ar.unshift(temp);
		} else {
			const temp = ar.shift();
			ar.push(temp);
		}

	}
	return ar
}

function secondMax(ar = []) {
	if (!ar.length) {
		throw 'Error!'
	} else {
		let rs = [...new Set(ar)].sort((a, b) => a > b ? 1 : -1);
		return rs.length > 1 ? rs[rs.length - 2] : rs[rs.length - 1];
	}
}

function fizzBuzz(number = 3) {
		let div3 = Number(number%3 != 0) || 'Fizz'
		let div5 = Number(number%5 != 0) || 'Buzz'
		return `${div3}${div5}`.replace(/\d/gim,'')
}

function fizzBuzz2(number = 3) {
		let msg = ''
		let div3 = number%3
		let div5 = number%5
		switch (div3) {
			case 0: msg+= 'Fizz'
		}
		switch (div5) {
			case 0: msg+= 'Buzz'
		}
		return msg;
}

function fizzBuzz3(number = 3) {
		let msg = ''
		msg+= numDiv(number,3,'Fizz')
		msg+= nnumDiv(number,5,'Buzz')
		return msg;
}

function numDiv(number,div,msg = '') {
		let rs = '';
		switch (number%div) {
			case 0: rs+= msg
		}
		return rs;
}